/*
						 LUFA Library
		 Copyright (C) Dean Camera, 2017.

	dean [at] fourwalledcubicle [dot] com
					 www.lufa-lib.org
*/

/*
	Copyright 2017	Dean Camera (dean [at] fourwalledcubicle [dot] com)

	Permission to use, copy, modify, distribute, and sell this
	software and its documentation for any purpose is hereby granted
	without fee, provided that the above copyright notice appear in
	all copies and that both that the copyright notice and this
	permission notice and warranty disclaimer appear in supporting
	documentation, and that the name of the author not be used in
	advertising or publicity pertaining to distribution of the
	software without specific, written prior permission.

	The author disclaims all warranties with regard to this
	software, including all implied warranties of merchantability
	and fitness.	In no event shall the author be liable for any
	special, indirect or consequential damages or any damages
	whatsoever resulting from loss of use, data or profits, whether
	in an action of contract, negligence or other tortious action,
	arising out of or in connection with the use or performance of
	this software.
*/

/** \file
 *
 *	Main source file for the FabScope firmware, derived from the LUFA
 *	VirtualSerial demo.
 */

#include "main.h"
#include <LUFA/Drivers/Peripheral/SPI.h>

#define BUTTONPORT PORTC
#define BUTTONPINOFFSET 3
#define BUTTONPINCTRL PORTC_PIN3CTRL

#define ADC_RVSPIN 1
#define ADC_RVSPINCTRL PORTC_PIN1CTRL

/** LUFA CDC Class driver interface configuration and state information. This structure is
 *	passed to all CDC Class driver functions, so that multiple instances of the same class
 *	within a device can be differentiated from one another.
 */
USB_ClassInfo_CDC_Device_t VirtualSerial_CDC_Interface =
	{
		.Config =
			{
				.ControlInterfaceNumber	 = INTERFACE_ID_CDC_CCI,
				.DataINEndpoint					 =
					{
						.Address					= CDC_TX_EPADDR,
						.Size						 = CDC_TXRX_EPSIZE,
						.Banks						= 1,
					},
				.DataOUTEndpoint =
					{
						.Address					= CDC_RX_EPADDR,
						.Size						 = CDC_TXRX_EPSIZE,
						.Banks						= 1,
					},
				.NotificationEndpoint =
					{
						.Address					= CDC_NOTIFICATION_EPADDR,
						.Size						 = CDC_NOTIFICATION_EPSIZE,
						.Banks						= 1,
					},
			},
	};

/** Standard file stream for the CDC interface when set up, so that the virtual CDC COM port can be
 *	used like any regular character stream in the C APIs.
 */
static FILE USBSerialStream;


/* button latch state variable */
static bool buttonState;
static uint16_t buttonDebounce;

#define BUTTONDEBOUNCETHRESH 10


#define ADC_BUFSZ 512
static uint16_t adc_buf[ADC_BUFSZ];

static bool doUSBTask = false;


/** Main program entry point. This routine contains the overall program flow, including initial
 *	setup of all components and the main program loop.
 */
int main(void)
{
	buttonState = false;
	buttonDebounce = 0;

	SetupHardware();

	/* Create a regular character stream for the interface so that it can be used with the stdio.h functions */
	CDC_Device_CreateStream(&VirtualSerial_CDC_Interface, &USBSerialStream);

	GlobalInterruptEnable();

	for (;;)
	{

		// consume input bytes to prevent blocking (don't do anything with them yet)
		CDC_Device_ReceiveByte(&VirtualSerial_CDC_Interface);

		// take an ADC reading and emit it over serial
		ADC_Read_Block();
		fputc('A', &USBSerialStream);
		fputc('A', &USBSerialStream);
		//fwrite(adc_buf, sizeof(uint16_t), ADC_BUFSZ, &USBSerialStream);
		for(int i = 0; i < ADC_BUFSZ; i++) {
			fwrite(adc_buf + i, sizeof(uint16_t), 1, &USBSerialStream);
		}
		fputc('Z', &USBSerialStream);
		fputc('Z', &USBSerialStream);

		if(doUSBTask) {
			CDC_Device_USBTask(&VirtualSerial_CDC_Interface);
			USB_USBTask();
			doUSBTask = false;
			PORTC.OUT ^= (1 << 0);
		}

	}
}

uint8_t ReadSignatureByte(uint16_t Address)
{
		NVM_CMD = NVM_CMD_READ_CALIB_ROW_gc;
		uint8_t Result;
		Result = pgm_read_byte((uint8_t *)Address);
		NVM_CMD = NVM_CMD_NO_OPERATION_gc;
		return Result;
}

void ADC_Init()
{

	/* set up CS / MOSI / SCK pin as output */
	PORTC.DIR |= (1 << 4) | (1 << 5) | (1 << 7);
	PORTC.OUT |= (1 << 4);

	/* set up RVS line as input */
	PORTC.DIR &= ~(1 << ADC_RVSPIN);
	ADC_RVSPINCTRL = PORT_ISC_BOTHEDGES_gc;

	SPI_Init(&SPIC, SPI_SPEED_FCPU_DIV_2 | SPI_ORDER_MSB_FIRST | SPI_SCK_LEAD_RISING |
                  SPI_SAMPLE_LEADING | SPI_MODE_MASTER);

	/* "flush" the SPI line by one frame */
	while(!(PORTC.IN &= (1 << ADC_RVSPIN)));
	PORTC.OUT &= ~(1 << 4);
	SPI_ReceiveByte(&SPIC);
	SPI_ReceiveByte(&SPIC);
	SPI_ReceiveByte(&SPIC);
	SPI_ReceiveByte(&SPIC);
	PORTC.OUT |= (1 << 4);
}

static uint8_t spi_rx_buf[4];

__attribute__((always_inline)) inline void ADC_Read(uint8_t * dest)
{
	while(!(PORTC.IN &= (1 << ADC_RVSPIN)));
	PORTC.OUT &= ~(1 << 4);
	*(dest + 1) = SPI_ReceiveByte(&SPIC);
	*dest = SPI_ReceiveByte(&SPIC);
	//spi_rx_buf[1] = SPI_ReceiveByte(&SPIC);
	//spi_rx_buf[0] = SPI_ReceiveByte(&SPIC);
	PORTC.OUT |= (1 << 4);
}

void ADC_Read_Block(void)
{
	TCC0.INTCTRLA = TC_OVFINTLVL_OFF_gc;
	// PORTC.OUT &= ~(1 << 0);

	for(uint8_t * p = (uint8_t*)adc_buf; p < (uint8_t*)(adc_buf + ADC_BUFSZ); p += 2)
	{
		ADC_Read(p);
	}
	// PORTC.OUT |= (1 << 0);
	TCC0.INTCTRLA = TC_OVFINTLVL_LO_gc;
}

ISR(TCC0_OVF_vect)
{
	doUSBTask = true;
}

/** Configures the board hardware and chip peripherals for the demo's functionality. */
void SetupHardware(void)
{
	/* Start the PLL to multiply the 2MHz RC oscillator to 32MHz and switch the CPU core to run from it */
	XMEGACLK_StartPLL(CLOCK_SRC_INT_RC2MHZ, 2000000, F_CPU);
	XMEGACLK_SetCPUClockSource(CLOCK_SRC_PLL);

	/* Start the 32MHz internal RC oscillator and start the DFLL to increase it to 48MHz using the USB SOF as a reference */
	XMEGACLK_StartInternalOscillator(CLOCK_SRC_INT_RC32MHZ);
	XMEGACLK_StartDFLL(CLOCK_SRC_INT_RC32MHZ, DFLL_REF_INT_USBSOF, F_USB);

	PMIC.CTRL = PMIC_LOLVLEN_bm | PMIC_MEDLVLEN_bm | PMIC_HILVLEN_bm;

	/* Hardware Initialization */
	USB_Init();
	ADC_Init();

	/* turn on main LED */
	PORTC.DIR |= (1 << 0);
	PORTC.OUT |= (1 << 0);

	/* set up button as input */
	BUTTONPORT.DIR &= ~(1 << BUTTONPINOFFSET);
	PORTC_PIN3CTRL = (1 << 6) | PORT_OPC_PULLUP_gc | PORT_ISC_BOTHEDGES_gc;

	/* set up 25ms USB interrupt */
	TCC0.CTRLA = TC_CLKSEL_DIV256_gc;
	TCC0.PER = F_CPU / 256 / 40;
	TCC0.INTCTRLA = TC_OVFINTLVL_LO_gc;
	TCC0.INTFLAGS = 0x01;
	TCC0.CTRLB = TC_WGMODE_NORMAL_gc;
}

/** Event handler for the library USB Connection event. */
void EVENT_USB_Device_Connect(void)
{
	//LEDs_SetAllLEDs(LEDMASK_USB_ENUMERATING);
}

/** Event handler for the library USB Disconnection event. */
void EVENT_USB_Device_Disconnect(void)
{
	//LEDs_SetAllLEDs(LEDMASK_USB_NOTREADY);
}

/** Event handler for the library USB Configuration Changed event. */
void EVENT_USB_Device_ConfigurationChanged(void)
{
	bool ConfigSuccess = true;

	ConfigSuccess &= CDC_Device_ConfigureEndpoints(&VirtualSerial_CDC_Interface);

	//LEDs_SetAllLEDs(ConfigSuccess ? LEDMASK_USB_READY : LEDMASK_USB_ERROR);
}

/** Event handler for the library USB Control Request reception event. */
void EVENT_USB_Device_ControlRequest(void)
{
	CDC_Device_ProcessControlRequest(&VirtualSerial_CDC_Interface);
}

/** CDC class driver callback function the processing of changes to the virtual
 *	control lines sent from the host..
 *
 *	\param[in] CDCInterfaceInfo	Pointer to the CDC class interface configuration structure being referenced
 */
void EVENT_CDC_Device_ControLineStateChanged(USB_ClassInfo_CDC_Device_t *const CDCInterfaceInfo)
{
	/* You can get changes to the virtual CDC lines in this callback; a common
		 use-case is to use the Data Terminal Ready (DTR) flag to enable and
		 disable CDC communications in your application when set to avoid the
		 application blocking while waiting for a host to become ready and read
		 in the pending data from the USB endpoints.
	*/
	bool HostReady = (CDCInterfaceInfo->State.ControlLineStates.HostToDevice & CDC_CONTROL_LINE_OUT_DTR) != 0;
}
