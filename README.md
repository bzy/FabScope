# FabScope

Coordination and discussion for the FabScope project.

## Requirements

* LUFA 170418 (http://www.fourwalledcubicle.com/LUFA.php)
* a PDI programmer (e.g. avrisp2)
* optionally, the Atmel DFU bootloader (AVR1916)
