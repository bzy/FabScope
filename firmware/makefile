#
#             LUFA Library
#     Copyright (C) Dean Camera, 2017.
#
#  dean [at] fourwalledcubicle [dot] com
#           www.lufa-lib.org
#
# --------------------------------------
#         LUFA Project Makefile.
# --------------------------------------

# Run "make help" for target help.

MCU          = atxmega16a4u
ARCH         = XMEGA
BOARD        = USBKEY
F_CPU        = 32000000
F_USB        = 48000000
OPTIMIZATION = s
TARGET       = main
SRC          = $(TARGET).c Descriptors.c $(LUFA_SRC_USB) $(LUFA_SRC_USBCLASS)
LUFA_PATH    = /PATH/TO/LUFA
CC_FLAGS     = -DUSE_LUFA_CONFIG_HEADER -IConfig/
LD_FLAGS     =

# Default target
all:

# Include LUFA-specific DMBS extension modules
DMBS_LUFA_PATH ?= $(LUFA_PATH)/Build/LUFA
include $(DMBS_LUFA_PATH)/lufa-sources.mk
include $(DMBS_LUFA_PATH)/lufa-gcc.mk

# Include common DMBS build system modules
DMBS_PATH      ?= $(LUFA_PATH)/Build/DMBS/DMBS
include $(DMBS_PATH)/core.mk
include $(DMBS_PATH)/cppcheck.mk
include $(DMBS_PATH)/doxygen.mk
include $(DMBS_PATH)/dfu.mk
include $(DMBS_PATH)/gcc.mk
include $(DMBS_PATH)/hid.mk
include $(DMBS_PATH)/avrdude.mk
include $(DMBS_PATH)/atprogram.mk

program-dfu: $(TARGET).hex
	sudo dfu-programmer atxmega16a4u erase
	sudo dfu-programmer atxmega16a4u flash $(TARGET).hex
	sudo dfu-programmer atxmega16a4u launch

program-avrisp2: $(TARGET).hex
	avrdude -p $(MCU) -c avrisp2 -U flash:w:$(TARGET).hex

program-avrisp2-dfu-fuses:
	avrdude -p $(MCU) -c avrisp2 -U fuse2:w:0xBF:m
