/*
						 LUFA Library
		 Copyright (C) Dean Camera, 2017.

	dean [at] fourwalledcubicle [dot] com
					 www.lufa-lib.org
*/

/*
	Copyright 2017	Dean Camera (dean [at] fourwalledcubicle [dot] com)

	Permission to use, copy, modify, distribute, and sell this
	software and its documentation for any purpose is hereby granted
	without fee, provided that the above copyright notice appear in
	all copies and that both that the copyright notice and this
	permission notice and warranty disclaimer appear in supporting
	documentation, and that the name of the author not be used in
	advertising or publicity pertaining to distribution of the
	software without specific, written prior permission.

	The author disclaims all warranties with regard to this
	software, including all implied warranties of merchantability
	and fitness.	In no event shall the author be liable for any
	special, indirect or consequential damages or any damages
	whatsoever resulting from loss of use, data or profits, whether
	in an action of contract, negligence or other tortious action,
	arising out of or in connection with the use or performance of
	this software.
*/

/** \file
 *
 *	Main source file for the FabScope firmware, derived from the LUFA
 *	VirtualSerial demo.
 */

#include "main.h"

#define BUTTONPORT PORTC
#define BUTTONPINOFFSET 3
#define BUTTONPINCTRL PORTC_PIN3CTRL

/** LUFA CDC Class driver interface configuration and state information. This structure is
 *	passed to all CDC Class driver functions, so that multiple instances of the same class
 *	within a device can be differentiated from one another.
 */
USB_ClassInfo_CDC_Device_t VirtualSerial_CDC_Interface =
	{
		.Config =
			{
				.ControlInterfaceNumber	 = INTERFACE_ID_CDC_CCI,
				.DataINEndpoint					 =
					{
						.Address					= CDC_TX_EPADDR,
						.Size						 = CDC_TXRX_EPSIZE,
						.Banks						= 1,
					},
				.DataOUTEndpoint =
					{
						.Address					= CDC_RX_EPADDR,
						.Size						 = CDC_TXRX_EPSIZE,
						.Banks						= 1,
					},
				.NotificationEndpoint =
					{
						.Address					= CDC_NOTIFICATION_EPADDR,
						.Size						 = CDC_NOTIFICATION_EPSIZE,
						.Banks						= 1,
					},
			},
	};

/** Standard file stream for the CDC interface when set up, so that the virtual CDC COM port can be
 *	used like any regular character stream in the C APIs.
 */
static FILE USBSerialStream;


/* button latch state variable */
static bool buttonState;
static uint16_t buttonDebounce;

#define BUTTONDEBOUNCETHRESH 10


/** Main program entry point. This routine contains the overall program flow, including initial
 *	setup of all components and the main program loop.
 */
int main(void)
{
	buttonState = false;
	buttonDebounce = 0;

	SetupHardware();

	/* Create a regular character stream for the interface so that it can be used with the stdio.h functions */
	CDC_Device_CreateStream(&VirtualSerial_CDC_Interface, &USBSerialStream);

	GlobalInterruptEnable();

	for (;;)
	{

		// consume input bytes to prevent blocking (don't do anything with them yet)
		CDC_Device_ReceiveByte(&VirtualSerial_CDC_Interface);

		// take an ADC reading and emit it over serial
		int16_t adcReading = ADC_Read();
		fprintf(&USBSerialStream, "%d\r\n", adcReading);

		CDC_Device_USBTask(&VirtualSerial_CDC_Interface);
		USB_USBTask();
	}
}

uint8_t ReadSignatureByte(uint16_t Address)
{
		NVM_CMD = NVM_CMD_READ_CALIB_ROW_gc;
		uint8_t Result;
		Result = pgm_read_byte((uint8_t *)Address);
		NVM_CMD = NVM_CMD_NO_OPERATION_gc;
		return Result;
}

void ADC_Init()
{
	// Based on advice from http://barefootelectronics.com/xMegaADC.aspx

	// Configure PA0 as input
	PORTA.DIR &= ~(1 << 0);
	// Configure PA0 as non-inverting totem pole with disabled digital input buf
	PORTA_PIN0CTRL = 7;

	ADCA.CTRLA = ADC_ENABLE_bm; // enabled, no DMA, no started conversion, no flush
	ADCA.CTRLB = (1<<4); // high impedance, no limit, signed, non-freerunning, 12-bit right
	ADCA.REFCTRL = (ADC_REFSEL1_bm) | (ADC_REFSEL0_bm); // AREFB
	ADCA.EVCTRL = 0 ; // no events
	ADCA.PRESCALER = ADC_PRESCALER_DIV64_gc ;
	ADCA.CALL = ReadSignatureByte(0x20) ; //ADC Calibration Byte 0
	ADCA.CALH = ReadSignatureByte(0x21) ; //ADC Calibration Byte 1

	_delay_us(400); // Wait at least 25 clocks

	ADCA.CH0.CTRL = ADC_CH_GAIN_4X_gc | ADC_CH_INPUTMODE_DIFFWGAIN_gc;
	ADCA.CH0.MUXCTRL = ADC_CH_MUXPOS_PIN0_gc | ADC_CH_MUXNEG_PIN4_gc;
	ADCA.CH0.INTCTRL = 0; // no interrupt
}

int32_t ADC_Read(void)
{
	int32_t res = 0;

	// throw away 1 reading and take the next one

	//for(uint8_t i = 0; i < 1; i++) {
		ADCA.CH0.CTRL |= ADC_CH_START_bm; // Start conversion
		while (ADCA.INTFLAGS==0) ; // Wait for complete
		ADCA.INTFLAGS = ADCA.INTFLAGS ; // writing 1 to INTFLAGS clears it
	//}

	//for(uint8_t i = 0; i < 1; i++) {
		ADCA.CH0.CTRL |= ADC_CH_START_bm; // Start conversion
		while (ADCA.INTFLAGS==0) ; // Wait for complete
		ADCA.INTFLAGS = ADCA.INTFLAGS ; // writing 1 to INTFLAGS clears it
		res = ADCA.CH0RES;
	//}
	return res;
}

/** Configures the board hardware and chip peripherals for the demo's functionality. */
void SetupHardware(void)
{
	/* Start the PLL to multiply the 2MHz RC oscillator to 32MHz and switch the CPU core to run from it */
	XMEGACLK_StartPLL(CLOCK_SRC_INT_RC2MHZ, 2000000, F_CPU);
	XMEGACLK_SetCPUClockSource(CLOCK_SRC_PLL);

	/* Start the 32MHz internal RC oscillator and start the DFLL to increase it to 48MHz using the USB SOF as a reference */
	XMEGACLK_StartInternalOscillator(CLOCK_SRC_INT_RC32MHZ);
	XMEGACLK_StartDFLL(CLOCK_SRC_INT_RC32MHZ, DFLL_REF_INT_USBSOF, F_USB);

	PMIC.CTRL = PMIC_LOLVLEN_bm | PMIC_MEDLVLEN_bm | PMIC_HILVLEN_bm;

	/* Hardware Initialization */
	USB_Init();
	ADC_Init();

	/* turn on main LED */
	PORTC.DIR |= (1 << 5);
	PORTC.OUT |= (1 << 5);

	/* set up button as input */
	BUTTONPORT.DIR &= ~(1 << BUTTONPINOFFSET);
	PORTC_PIN3CTRL = (1 << 6) | PORT_OPC_PULLUP_gc | PORT_ISC_BOTHEDGES_gc;
}

/** Event handler for the library USB Connection event. */
void EVENT_USB_Device_Connect(void)
{
	//LEDs_SetAllLEDs(LEDMASK_USB_ENUMERATING);
}

/** Event handler for the library USB Disconnection event. */
void EVENT_USB_Device_Disconnect(void)
{
	//LEDs_SetAllLEDs(LEDMASK_USB_NOTREADY);
}

/** Event handler for the library USB Configuration Changed event. */
void EVENT_USB_Device_ConfigurationChanged(void)
{
	bool ConfigSuccess = true;

	ConfigSuccess &= CDC_Device_ConfigureEndpoints(&VirtualSerial_CDC_Interface);

	//LEDs_SetAllLEDs(ConfigSuccess ? LEDMASK_USB_READY : LEDMASK_USB_ERROR);
}

/** Event handler for the library USB Control Request reception event. */
void EVENT_USB_Device_ControlRequest(void)
{
	CDC_Device_ProcessControlRequest(&VirtualSerial_CDC_Interface);
}

/** CDC class driver callback function the processing of changes to the virtual
 *	control lines sent from the host..
 *
 *	\param[in] CDCInterfaceInfo	Pointer to the CDC class interface configuration structure being referenced
 */
void EVENT_CDC_Device_ControLineStateChanged(USB_ClassInfo_CDC_Device_t *const CDCInterfaceInfo)
{
	/* You can get changes to the virtual CDC lines in this callback; a common
		 use-case is to use the Data Terminal Ready (DTR) flag to enable and
		 disable CDC communications in your application when set to avoid the
		 application blocking while waiting for a host to become ready and read
		 in the pending data from the USB endpoints.
	*/
	bool HostReady = (CDCInterfaceInfo->State.ControlLineStates.HostToDevice & CDC_CONTROL_LINE_OUT_DTR) != 0;
}
