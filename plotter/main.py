#!/usr/bin/python

import matplotlib as mpl
mpl.use('TKAgg')
import serial
import Tkinter as tk
import numpy as np
import matplotlib.backends.tkagg as tkagg
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
import time

s = serial.Serial(port="/dev/ttyACM0", baudrate=115200)

# Create a canvas
w, h = 600, 400
window = tk.Tk()
window.title("A figure in a canvas")

# Create the figure we desire to add to an existing canvas
fig = mpl.figure.Figure(figsize=(6, 4), dpi=192)
ax = fig.add_axes([0, 0, 1, 1])

canvas = FigureCanvasTkAgg(fig, master=window)
canvas.show()
canvas.get_tk_widget().pack(side=tk.TOP, fill=tk.BOTH, expand=1)
canvas._tkcanvas.pack(side=tk.TOP, fill=tk.BOTH, expand=1)

# Add more elements to the canvas, potentially on top of the figure
# canvas.create_line(200, 50, fig_x + fig_w / 2, fig_y + fig_h / 2)
# canvas.create_text(200, 50, text="Zero-crossing", anchor="s")

#buf = np.zeros(512)

def idle(parent, canvas):
    global ax, s

    while True:
        while s.read(1) != 'A':
            pass
        if s.read(1) != 'A':
            continue

        tmp = s.read(1024)

        if s.read(2) != 'ZZ':
            continue

        buf = np.frombuffer(tmp, dtype=np.uint16, count=512)
        buf = buf >> 4
        break

    ax.clear()
    X = np.linspace(0, 511, 512)
    Y = buf
    ax.plot(X, Y)
    ax.set_xlim(0, 511)
    ax.set_ylim(0, 4095)

    canvas.draw()
    parent.after(33, idle, parent, canvas)

# Let Tk take over
window.after(100,idle,window,canvas)
tk.mainloop()
